#-------------------------------------------------
#
# Project created by QtCreator 2014-06-08T18:11:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CargoDistributionSimulation
TEMPLATE = app


SOURCES += src/main.cpp\
    src/gui/widget.cpp \
    src/gui/cart/abstractcartview.cpp \
    src/gui/cart/workshopcartview.cpp \
    src/gui/cart/warehousecartview.cpp \
    src/gui/mnemowidget.cpp \
    src/settings.cpp \
    src/gui/settingswidget.cpp \
    src/gui/taskwidget.cpp \
    src/controller.cpp \
    src/taskmanager.cpp \
    src/cart.cpp \
    src/gui/regwidget.cpp \
    src/gui/controlwidget.cpp \
    src/gui/myspinbox.cpp \
    src/gui/simcontrolwidget.cpp

HEADERS  += src/gui/widget.h \
    src/gui/cart/abstractcartview.h \
    src/gui/cart/workshopcartview.h \
    src/gui/cart/warehousecartview.h \
    src/gui/mnemowidget.h \
    src/defs.h \
    src/settings.h \
    src/gui/settingswidget.h \
    src/gui/taskwidget.h \
    src/controller.h \
    src/taskmanager.h \
    src/cart.h \
    src/gui/regwidget.h \
    src/gui/controlwidget.h \
    src/gui/myspinbox.h \
    src/gui/simcontrolwidget.h

RESOURCES += \
    res.qrc
