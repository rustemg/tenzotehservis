#include "controller.h"
#include "src/defs.h"
#include "src/settings.h"

Controller::Controller(QObject *parent) :
    QObject(parent),
    timer(this)
{
    widget = new Widget();
    widget->show();

    tm = new TaskManager(widget->taskWidget, this);

    for (int i = 0; i < 5; i++) {
        carts.append(new Cart(i, widget->mnemoWidget->cartViews[i], this));
    }

    currentTask = 0;
    futureTask = 0;

    timer.setInterval(1000);
    connect(&timer, SIGNAL(timeout()), SLOT(iterate()));
    connect(&Settings::current(), SIGNAL(stateChanged(int,int)),
            SLOT(processStateChange(int,int)));
}

void Controller::iterate()
{
    switch(Settings::current().getMode()) {
    case Settings::MODE_NEO:
        newIteration();
        break;
    case Settings::MODE_STANDART:
        standartIteration();
        break;
    }
}


void Controller::standartIteration()
{
    if (tm->taskCount() == 0) {
        return;
    }

    Cart* wCart = carts[0];

    if (wCart->getState() == CartState::DownloadFinished) {
        currentTask = 0;
        tm->removeCurrentTask();
        carts[0]->startMove(Y_POS_0);
    }

    if (currentTask == 0) {
        currentTask = tm->getCurrentTask();
        if (currentTask == 0) {
            return;
        }
    }

    futureTask = tm->getFutureTask();

    if (wCart->getState() == CartState::NoJob
            && wCart->getPosition() == Y_POS_0) {
        wCart->uploadCargo();
    }

    if (wCart->getState() == CartState::UploadFinished) {
        wCart->startMove(yPos(currentTask));
    }

    Cart* cCart = carts[currentTask];
    if (cCart->getState() == CartState::NoJob
            || (cCart->getState() == CartState::Waiting
                && cCart->getPosition() == X_POS_MIDDLE)) {
        cCart->startMove(X_POS_END);
    }

    if (wCart->getState() == CartState::Waiting
            && wCart->getPosition() == yPos(currentTask)
            && cCart->getState() == CartState::Waiting
            && cCart->getPosition() == X_POS_END) {
        carts[0]->downloadCargo();
        cCart->uploadCargo();
    }

    if (futureTask != 0 && futureTask != currentTask) {
        Cart* fCart = carts[futureTask];
        if (fCart->getState() == CartState::NoJob) {
            fCart->startMove(X_POS_MIDDLE);
        }
    }
}

void Controller::newIteration()
{
    if (tm->taskCount() == 0) {
        return;
    }

    Cart* wCart = carts[0];

    if (wCart->getState() == CartState::DownloadFinished) {
        currentTask = 0;
        tm->removeCurrentTask();
        carts[0]->startMove(Y_POS_0);
    }

    if (currentTask == 0) {
        currentTask = tm->getCurrentTask();
        if (currentTask == 0) {
            return;
        }
    }

    if (wCart->getState() == CartState::NoJob
            && wCart->getPosition() == Y_POS_0) {
        wCart->uploadCargo();
    }

    if (wCart->getState() == CartState::UploadFinished) {
        wCart->startMove(yPos(currentTask));
    }

    Cart* cCart = carts[currentTask];
    if (cCart->getState() == CartState::NoJob
            || (cCart->getState() == CartState::Waiting
                && (cCart->getPosition() == X_POS_MID_2
                    || cCart->getPosition() == X_POS_MID_1))) {
        cCart->startMove(X_POS_END);
    }


    if (wCart->getState() == CartState::Waiting
            && wCart->getPosition() == yPos(currentTask)
            && cCart->getState() == CartState::Waiting
            && cCart->getPosition() == X_POS_END) {
        carts[0]->downloadCargo();
        cCart->uploadCargo();
    }

    mid1.clear();
    mid2.clear();
    for (int i = 1; i < 5; i++) {
        if (carts[i]->getTarget() == X_POS_MID_2) {
            mid2.insert(i);
        }

        if (carts[i]->getTarget() == X_POS_MID_1) {
            mid1.insert(i);
        }
    }

    futureTasks = tm->getFutureTasks();
    for (int j = 0; j < futureTasks.size(); j++) {
        if (futureTasks[j] != 0 && futureTasks[j] != currentTask) {
            if (mid2.count() < Settings::current().getCartCountAtAChekpoint(1)
                    && !mid2.contains(futureTasks[j])) {
                Cart* fCart = carts[futureTasks[j]];
                if (fCart->getPosition() < X_POS_END
                        && (fCart->getState() == CartState::NoJob
                            || fCart->getState() == CartState::Waiting)) {
                    mid2.insert(futureTasks[j]);
                    fCart->startMove(X_POS_MID_2);
                }
            }
        }

        for (int j = 0; j < futureTasks.size(); j++) {
            if (futureTasks[j] != 0 && futureTasks[j] != currentTask) {
                if (mid1.count() < Settings::current().getCartCountAtAChekpoint(0)
                        && !mid1.contains(futureTasks[j])
                        && !mid2.contains(futureTasks[j])) {
                    Cart* fCart = carts[futureTasks[j]];
                    if (fCart->getPosition() < X_POS_MID_2
                            && (fCart->getState() == CartState::NoJob
                                || fCart->getState() == CartState::Waiting)) {
                        mid1.insert(futureTasks[j]);
                        fCart->startMove(X_POS_MID_1);
                    }
                }
            }
        }
    }
}

void Controller::processStateChange(int oldValue, int newValue)
{
    Q_UNUSED(oldValue)
    switch(newValue) {
    case Settings::STATE_RUN:
        timer.start();
        break;
    case Settings::STATE_PAUSE:
        timer.stop();
        break;
    case Settings::STATE_STOP:
        timer.stop();
        currentTask = 0;
        futureTask = 0;
        mid1.clear();
        mid2.clear();
        futureTasks.clear();
        break;
    }
}


int Controller::yPos(int warehouse)
{
    return widget->mnemoWidget->yPositions[warehouse];
}
