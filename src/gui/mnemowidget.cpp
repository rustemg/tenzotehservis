#include "mnemowidget.h"
#include "src/defs.h"
#include "src/settings.h"

#define CART_COUNT 5

MnemoWidget::MnemoWidget(QWidget *parent) :
    QLabel(parent)
{
    setPixmap(QPixmap(":/img/bg.png"));

    yPositions.append(Y_POS_0);
    yPositions.append(Y_POS_1);
    yPositions.append(Y_POS_2);
    yPositions.append(Y_POS_3);
    yPositions.append(Y_POS_4);

    AbstractCartView* cartView;

    cartView = new WarehouseCartView(this);
    cartView->move(320, yPositions[0]);
    cartViews.append(cartView);

    for (int i = 1; i < CART_COUNT; i++) {
        cartView = new WorkshopCartView(this);
        cartView->move(X_POS_BEGIN, yPositions[i]);
        cartViews.append(cartView);
    }

    lblA = new QLabel("<h2>A</h2>", this);
    lblA->move(155, 120);
    lblB = new QLabel("<h2>B</h2>", this);
    lblB->move(240, 120);

    connect(&Settings::current(), SIGNAL(stateChanged(int,int)),
            SLOT(processSimStateChange(int,int)));
}

void MnemoWidget::processSimStateChange(int oldValue, int newValue)
{
    Q_UNUSED(oldValue)
    if (newValue == Settings::STATE_STOP) {
        lblA->show();
        lblB->show();
    } else {
        lblA->hide();
        lblB->hide();
    }
}
