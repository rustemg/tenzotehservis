#include "controlwidget.h"
#include "src/settings.h"

ControlWidget::ControlWidget(QWidget *parent) :
    QWidget(parent)
{
    simControlWidget = new SimControlWidget();

    rbStandard = new QRadioButton("Стандартный");
    rbNeo = new QRadioButton("Новый");
    rbNeo->setChecked(true);
    connect(rbNeo, SIGNAL(toggled(bool)), SLOT(processModeChange(bool)));
    connect(rbStandard, SIGNAL(toggled(bool)), SLOT(processModeChange(bool)));

    QHBoxLayout* countLayout = new QHBoxLayout();
    for (int i = 0; i < 2; i++) {
        MySpinBox* spinBox = new MySpinBox("Точка " + QString(QChar(65 + i)), 0, 4);
        spinBox->setObjectName("checkpoint" + QString::number(i));
        countLayout->addWidget(spinBox);
        connect(spinBox, SIGNAL(valueChanged(int)), SLOT(processCountChange(int)));
    }
    countFrame = new QFrame();
    countFrame->setFrameStyle(QFrame::Box | QFrame::Sunken);
    QVBoxLayout* vCountLayout = new QVBoxLayout();
    vCountLayout->addWidget(new QLabel("<h3>Макс кол-во телег на точках ожидания</h3>"));
    vCountLayout->addLayout(countLayout);
    countFrame->setLayout(vCountLayout);


    QHBoxLayout* rbLayout = new QHBoxLayout();
    rbLayout->addWidget(rbStandard);
    rbLayout->addWidget(rbNeo);

    QVBoxLayout* vRbLayout = new QVBoxLayout();
    vRbLayout->addWidget(new QLabel("<h3>Режим работы</h3>"));
    vRbLayout->addLayout(rbLayout);

    rbFrame = new QFrame();
    rbFrame->setFrameStyle(QFrame::Box | QFrame::Sunken);
    rbFrame->setLayout(vRbLayout);

    QVBoxLayout* mainLayout  = new QVBoxLayout();
    mainLayout->addWidget(simControlWidget);
    mainLayout->addWidget(rbFrame);
    mainLayout->addWidget(countFrame);
    mainLayout->addStretch();

    setLayout(mainLayout);

    connect(&Settings::current(), SIGNAL(stateChanged(int,int)),
            SLOT(processStateChange(int,int)));
}

void ControlWidget::processCountChange(int value)
{
    QString s = sender()->objectName();
    int cp = s.right(1).toInt();
    Settings::current().setCartCountAtCheckpoint(cp, value);
}

void ControlWidget::processModeChange(bool value)
{
    if (value) {
        if (sender() == rbNeo) {
            countFrame->setEnabled(true);
            Settings::current().setMode(Settings::MODE_NEO);
        } else {
            countFrame->setEnabled(false);
            Settings::current().setMode(Settings::MODE_STANDART);
        }
    }
}

void ControlWidget::processStateChange(int oldValue, int newValue)
{
    Q_UNUSED(oldValue)
    rbFrame->setEnabled(newValue == Settings::STATE_STOP);
    countFrame->setEnabled(newValue == Settings::STATE_STOP);
}
