#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include "src/gui/mnemowidget.h"
#include "src/gui/settingswidget.h"
#include "src/gui/taskwidget.h"
#include "src/gui/controlwidget.h"

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);

    MnemoWidget* mnemoWidget;
    SettingsWidget* settingsWidget;
    TaskWidget* taskWidget;
    ControlWidget* controlWidget;
};

#endif // WIDGET_H
