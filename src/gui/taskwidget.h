#ifndef TASKWIDGET_H
#define TASKWIDGET_H

#include <QWidget>
#include <QListWidget>
#include <QPushButton>
#include <QLabel>
#include <QMenu>
#include <QHBoxLayout>
#include <QVBoxLayout>

class TaskManager;

class TaskWidget : public QWidget
{
    Q_OBJECT

    friend class TaskManager;

public:
    explicit TaskWidget(QWidget *parent = 0);

private slots:
    void addTask(QAction* action);
    void editTask(QAction* action);
    void deleteTask();
    void changeLabel();

private:
    QString getCartId(QAction* action);
    QMenu* getMenu();

private:
    QPushButton* btnAdd;
    QPushButton* btnEdit;
    QPushButton* btnDel;
    QListWidget* listWidget;
    QLabel* lbl;
};

#endif // TASKWIDGET_H
