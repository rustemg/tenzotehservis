#ifndef REGWIDGET_H
#define REGWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QSlider>
#include <QSpinBox>
#include <QHBoxLayout>

class RegWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RegWidget(const QString& label, int min, int max, QWidget *parent = 0);

    signals:
        void valueChanged(int);

public slots:
    void setValue(int value);
    void setMeasureUnit(const QString& unit);

private:
    QSlider* slider;
    QSpinBox* spinBox;
    QLabel* lbl;

};

#endif // REGWIDGET_H
