#ifndef SIMCONTROLWIDGET_H
#define SIMCONTROLWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>

class SimControlWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SimControlWidget(QWidget *parent = 0);

private slots:
    void processButtonClick();

private:
    QPushButton* btnRun;
    QPushButton* btnPause;
    QPushButton* btnStop;
    QLabel* lbl;
};

#endif // SIMCONTROLWIDGET_H
