#ifndef CONTROLWIDGET_H
#define CONTROLWIDGET_H

#include <QWidget>
#include <QFrame>
#include <QRadioButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "src/gui/myspinbox.h"
#include "src/gui/simcontrolwidget.h"

class ControlWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ControlWidget(QWidget *parent = 0);

private slots:
    void processCountChange(int value);
    void processModeChange(bool value);
    void processStateChange(int oldValue, int newValue);

private:
    QRadioButton* rbStandard;
    QRadioButton* rbNeo;
    QFrame* countFrame;
    QFrame* rbFrame;
    SimControlWidget* simControlWidget;
};

#endif // CONTROLWIDGET_H
