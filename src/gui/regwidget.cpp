#include "regwidget.h"

RegWidget::RegWidget(const QString &label, int min, int max, QWidget *parent) :
    QWidget(parent)
{
    slider = new QSlider(Qt::Horizontal);
    slider->setRange(min, max);
    spinBox = new QSpinBox();
    spinBox->setRange(min, max);
    lbl = new QLabel(label);

    QHBoxLayout* mainLayout = new QHBoxLayout();
    mainLayout->addWidget(lbl);
    mainLayout->addWidget(slider);
    mainLayout->addWidget(spinBox);

    setLayout(mainLayout);

    connect(slider, SIGNAL(valueChanged(int)), spinBox, SLOT(setValue(int)));
    connect(slider, SIGNAL(valueChanged(int)), SIGNAL(valueChanged(int)));
    connect(spinBox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
    connect(spinBox, SIGNAL(valueChanged(int)), SIGNAL(valueChanged(int)));
}

void RegWidget::setValue(int value)
{
    slider->setValue(value);
}

void RegWidget::setMeasureUnit(const QString &unit)
{
    spinBox->setSuffix(unit);
}
