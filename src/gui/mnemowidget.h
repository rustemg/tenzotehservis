#ifndef MNEMOWIDGET_H
#define MNEMOWIDGET_H

#include <QLabel>
#include <QVector>

#include "src/gui/cart/abstractcartview.h"
#include "src/gui/cart/warehousecartview.h"
#include "src/gui/cart/workshopcartview.h"

class MnemoWidget : public QLabel
{
    Q_OBJECT
public:
    explicit MnemoWidget(QWidget *parent = 0);
    QVector<AbstractCartView*> cartViews;
    QVector<int> yPositions;

private slots:
    void processSimStateChange(int oldValue, int newValue);

private:
    QLabel* lblA;
    QLabel* lblB;
};

#endif // MNEMOWIDGET_H
