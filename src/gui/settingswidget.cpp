#include "settingswidget.h"
#include "src/settings.h"

SettingsWidget::SettingsWidget(QWidget *parent) :
    QWidget(parent)
{
    QLabel* cartSpeedLabel = new QLabel("<h3>Скорость телег</h3>");
    QVBoxLayout* cartSpeedLayout = new QVBoxLayout();
    cartSpeedLayout->addWidget(cartSpeedLabel);

    for (int i = 0; i < 5; i++) {
        RegWidget* reg = new RegWidget((i == 0 ? "Склад" : "Цех " + QString::number(i)), 5, 40);
        reg->setValue(Settings::current().getCartSpeed(i));
        reg->setObjectName("sliderspeed" + QString::number(i));
        connect(reg, SIGNAL(valueChanged(int)), SLOT(processCartSpeedChange(int)));

        cartSpeedLayout->addWidget(reg);
    }

    QFrame* cartSpeedFrame = new QFrame();
    cartSpeedFrame->setFrameStyle(QFrame::Box | QFrame::Sunken);
    cartSpeedFrame->setLayout(cartSpeedLayout);

    QLabel* cartLoadSpeedLabel = new QLabel("<h3>Скорость загрузки/отгрузки складской телеги</h3>");
    QVBoxLayout* cartLoadSpeedLayout = new QVBoxLayout();
    cartLoadSpeedLayout->addWidget(cartLoadSpeedLabel);

    for (int i = 0; i < 2; i++) {
        RegWidget* reg = new RegWidget((i == 0 ? "Загрузка" : "Отгрузка"), 1, 10);
        reg->setObjectName("cartloadspeed" + QString::number(i));
        int v = 1;
        if (i == 0) {
            v = Settings::current().getWarehouseCartUploadSpeed();
        } else {
            v = Settings::current().getWarehouseCartDownloadSpeed();
        }
        reg->setValue((int)(v / 1000));
        reg->setMeasureUnit(" сек");
        connect(reg, SIGNAL(valueChanged(int)), SLOT(processCartLoadSpeed(int)));

        cartLoadSpeedLayout->addWidget(reg);
    }

    QFrame* cartLoadSpeedFrame = new QFrame();
    cartLoadSpeedFrame->setFrameStyle(QFrame::Box | QFrame::Sunken);
    cartLoadSpeedFrame->setLayout(cartLoadSpeedLayout);

    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainLayout->addWidget(cartSpeedFrame);
    mainLayout->addWidget(cartLoadSpeedFrame);

    setLayout(mainLayout);
}

void SettingsWidget::processCartSpeedChange(int value)
{
    int cargoId = sender()->objectName().right(1).toInt();
    Settings::current().setCartSpeed(cargoId, value);
}

void SettingsWidget::processCartLoadSpeed(int value)
{
    int id = sender()->objectName().right(1).toInt();
    if (id == 0) {
        Settings::current().setWarehouseCartUploadSpeed(value);
    } else {
        Settings::current().setWarehouseCartDownloadSpeed(value);
    }
}
