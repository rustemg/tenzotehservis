#ifndef MYSPINBOX_H
#define MYSPINBOX_H

#include <QWidget>
#include <QSpinBox>
#include <QLabel>
#include <QHBoxLayout>

class MySpinBox : public QWidget
{
    Q_OBJECT
public:
    explicit MySpinBox(const QString& label, int min, int max, QWidget *parent = 0);

signals:
    void valueChanged(int);

private:
    QSpinBox* spinBox;
    QLabel* lbl;
};

#endif // MYSPINBOX_H
