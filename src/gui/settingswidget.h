#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include <QFrame>
#include <QLabel>
#include <QVBoxLayout>

#include "src/gui/regwidget.h"


class SettingsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsWidget(QWidget *parent = 0);

private slots:
    void processCartSpeedChange(int value);
    void processCartLoadSpeed(int value);
};

#endif // SETTINGSWIDGET_H
