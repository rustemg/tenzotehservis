#include "taskwidget.h"

TaskWidget::TaskWidget(QWidget *parent) :
    QWidget(parent)
{
    listWidget = new QListWidget();
    connect(listWidget->model(), SIGNAL(rowsRemoved(QModelIndex,int,int)),
            SLOT(changeLabel()));
    connect(listWidget->model(), SIGNAL(rowsInserted(QModelIndex,int,int)),
            SLOT(changeLabel()));

    btnAdd = new QPushButton("Добавить");
    //btnEdit = new QPushButton("Редактировать");
    //btnDel = new QPushButton("Удалить");

    btnAdd->setMenu(getMenu());
    connect(btnAdd->menu(), SIGNAL(triggered(QAction*)), SLOT(addTask(QAction*)));

    lbl = new QLabel("<h2>Нет заданий</h2>");

    QHBoxLayout* buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(lbl);
    buttonsLayout->addWidget(btnAdd, 0, Qt::AlignRight);
    //buttonsLayout->addWidget(btnEdit);
    //buttonsLayout->addWidget(btnDel);

    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainLayout->addLayout(buttonsLayout);
    mainLayout->addWidget(listWidget);

    setLayout(mainLayout);


    new QListWidgetItem("Цех 4" , listWidget);
    new QListWidgetItem("Цех 1" , listWidget);
    new QListWidgetItem("Цех 3" , listWidget);
    new QListWidgetItem("Цех 2" , listWidget);
}

void TaskWidget::addTask(QAction* action)
{
    new QListWidgetItem("Цех " + getCartId(action), listWidget);
}

void TaskWidget::editTask(QAction *action)
{
    QList<QListWidgetItem*> items = listWidget->selectedItems();
    if (items.size() == 0) {
        return;
    }

    QString s = listWidget->currentItem()->text();
    s = s.replace(s.length() - 1, 1, getCartId(action));
}

void TaskWidget::deleteTask()
{

}

void TaskWidget::changeLabel()
{
    QListWidgetItem* item =  listWidget->item(0);
    QString txt;
    if (item == 0) {
        txt = "Нет заданий";
    } else {
        txt = item->text();
    }
    lbl->setText("<h2>" + txt + "</h2>");
}

QString TaskWidget::getCartId(QAction *action)
{
    return action->text().right(1);
}

QMenu* TaskWidget::getMenu()
{
    QMenu* menu = new QMenu();
    for (int i = 0; i < 4; i++) {
        menu->addAction("Цех " + QString::number(i + 1));
    }

    return menu;
}
