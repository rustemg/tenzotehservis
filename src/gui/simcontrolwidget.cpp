#include "simcontrolwidget.h"
#include "src/settings.h"

SimControlWidget::SimControlWidget(QWidget *parent) :
    QWidget(parent)
{
    btnRun = new QPushButton(QIcon(":/img/play.png"), "");
    btnPause = new QPushButton(QIcon(":/img/pause.png"), "");
    btnStop = new QPushButton(QIcon(":/img/stop.png"),"");

    connect(btnRun, SIGNAL(clicked()), SLOT(processButtonClick()));
    connect(btnPause, SIGNAL(clicked()), SLOT(processButtonClick()));
    connect(btnStop, SIGNAL(clicked()), SLOT(processButtonClick()));

    lbl = new QLabel("Остановлено");
    QFont font;
    font.setBold(true);
    lbl->setFont(font);

    QHBoxLayout* mainLayout = new QHBoxLayout();
    mainLayout->addWidget(btnRun, 0, Qt::AlignLeft);
    mainLayout->addWidget(btnPause, 0, Qt::AlignLeft);
    mainLayout->addWidget(btnStop, 0, Qt::AlignLeft);
    mainLayout->addStretch();
    mainLayout->addWidget(lbl);

    setLayout(mainLayout);
}

void SimControlWidget::processButtonClick()
{
    if (sender() == btnRun) {
        Settings::current().setSimState(Settings::STATE_RUN);
        lbl->setText("Работает");
    } else if (sender() == btnPause) {
        Settings::current().setSimState(Settings::STATE_PAUSE);
        lbl->setText("Пауза");
    } else if (sender() == btnStop) {
        Settings::current().setSimState(Settings::STATE_STOP);
        lbl->setText("Остановлено");
    }
}
