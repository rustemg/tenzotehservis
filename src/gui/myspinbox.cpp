#include "myspinbox.h"

MySpinBox::MySpinBox(const QString &label, int min, int max, QWidget *parent) :
    QWidget(parent)
{
    lbl = new QLabel(label);
    spinBox = new QSpinBox();
    spinBox->setRange(min, max);
    spinBox->setValue(1);

    QHBoxLayout* mainLayout = new QHBoxLayout();
    mainLayout->addWidget(lbl);
    mainLayout->addSpacing(10);
    mainLayout->addWidget(spinBox);
    mainLayout->addStretch();

    setLayout(mainLayout);

    connect(spinBox, SIGNAL(valueChanged(int)), SIGNAL(valueChanged(int)));
}
