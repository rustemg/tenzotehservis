#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    mnemoWidget = new MnemoWidget();
    settingsWidget = new SettingsWidget();
    taskWidget = new TaskWidget();
    controlWidget = new ControlWidget();

    QVBoxLayout* rightLayout = new QVBoxLayout();
    rightLayout->addWidget(taskWidget);
    rightLayout->addWidget(settingsWidget);

    QVBoxLayout* leftLayout = new QVBoxLayout();
    leftLayout->addWidget(mnemoWidget, 0, Qt::AlignLeft | Qt::AlignTop);
    leftLayout->addWidget(controlWidget);

    QHBoxLayout* mainLayout = new QHBoxLayout();
    mainLayout->addLayout(leftLayout);
    mainLayout->addLayout(rightLayout);

    setLayout(mainLayout);

    setWindowTitle("Газизов Рустем +7 965 604-74-34");
}
