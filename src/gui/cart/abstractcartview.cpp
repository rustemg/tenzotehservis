#include "abstractcartview.h"

AbstractCartView::AbstractCartView(QWidget *parent) :
    QLabel(parent)
{
    imgCargoStatus = new QLabel(this);
    imgCargoStatus->setFixedSize(20, 20);
    imgCargoStatus->move(imgCargoStatus->x() + 10, imgCargoStatus->y());
}

void AbstractCartView::showUploadIcon()
{
    imgCargoStatus->setPixmap(QPixmap(":/img/arrow_down.png"));
    imgCargoStatus->show();
}

void AbstractCartView::showDownloadIcon()
{
    imgCargoStatus->setPixmap(QPixmap(":/img/arrow_up.png"));
    imgCargoStatus->show();
}

void AbstractCartView::hideIcon()
{
    imgCargoStatus->hide();
}
