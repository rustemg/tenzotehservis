#include "workshopcartview.h"

WorkshopCartView::WorkshopCartView(QWidget *parent) :
    AbstractCartView(parent)
{
    setPixmap(QPixmap(":/img/rect.png"));
}

int WorkshopCartView::getPosition()
{
    return x();
}

void WorkshopCartView::setPosition(int pos)
{
    move(pos, y());
}
