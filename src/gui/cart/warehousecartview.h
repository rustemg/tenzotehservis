#ifndef WAREHOUSECARTVIEW_H
#define WAREHOUSECARTVIEW_H

#include "abstractcartview.h"

class WarehouseCartView : public AbstractCartView
{
    Q_OBJECT
public:
    explicit WarehouseCartView(QWidget *parent = 0);

    int getPosition();
        void setPosition(int pos);

};

#endif // WAREHOUSECARTVIEW_H
