#ifndef ABSTRACTCARTVIEW_H
#define ABSTRACTCARTVIEW_H

#include <QLabel>

class AbstractCartView : public QLabel
{
    Q_OBJECT
public:
    explicit AbstractCartView(QWidget *parent = 0);
    void showUploadIcon();
    void showDownloadIcon();
    void hideIcon();

    virtual int getPosition() = 0;
    virtual void setPosition(int pos) = 0;

private:
    QLabel* imgCargoStatus;

};

#endif // ABSTRACTCARTVIEW_H
