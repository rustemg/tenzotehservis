#include "warehousecartview.h"

WarehouseCartView::WarehouseCartView(QWidget *parent) :
    AbstractCartView(parent)
{
    setPixmap(QPixmap(":/img/rect.png"));
}

int WarehouseCartView::getPosition()
{
    return y();
}

void WarehouseCartView::setPosition(int pos)
{
    move(x(), pos);
}
