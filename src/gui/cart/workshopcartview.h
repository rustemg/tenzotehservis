#ifndef WORKSHOPCARTVIEW_H
#define WORKSHOPCARTVIEW_H

#include "abstractcartview.h"

class WorkshopCartView : public AbstractCartView
{
    Q_OBJECT
public:
    explicit WorkshopCartView(QWidget *parent = 0);

    int getPosition();
    void setPosition(int pos);

};

#endif // WORKSHOPCARTVIEW_H
