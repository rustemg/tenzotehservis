#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QTimer>
#include <QVector>
#include <QSet>

#include "src/cart.h"
#include "src/taskmanager.h"
#include "src/gui/widget.h"

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = 0);

private slots:
    void iterate();
    void processStateChange(int oldValue, int newValue);

private:
    void standartIteration();
    void newIteration();
    int yPos(int warehouse);

private:
    int currentTask;
    int futureTask;
    QVector<int> futureTasks;
    QVector<Cart*> carts;
    QSet<int> mid1;
    QSet<int> mid2;
    TaskManager* tm;
    QTimer timer;
    Widget* widget;
};

#endif // CONTROLLER_H
