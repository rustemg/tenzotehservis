#include "settings.h"

Settings::Settings(QObject *parent) :
    QObject(parent)
{
    for (int i = 0; i < 5; i++) {
        cartSpeeds.append(20);
    }

    warehouseCartDownloadSpeed = 2000;
    warehouseCartUploadSpeed = 2000;

    mode = Settings::MODE_NEO;
    simState = Settings::STATE_STOP;

    cartsCountAtCheckPoint.append(1);
    cartsCountAtCheckPoint.append(1);
}

int Settings::getWarehouseCartUploadSpeed() const
{
    return warehouseCartUploadSpeed;
}

void Settings::setWarehouseCartUploadSpeed(int value)
{
    warehouseCartUploadSpeed = value * 1000;
}
int Settings::getWarehouseCartDownloadSpeed() const
{
    return warehouseCartDownloadSpeed;
}

void Settings::setWarehouseCartDownloadSpeed(int value)
{
    warehouseCartDownloadSpeed = value * 1000;
}

int Settings::getCartSpeed(int cartId) const
{
    if (cartId >= cartSpeeds.size() || cartId < 0) {
        return 0;
    } else {
        return cartSpeeds[cartId];
    }
}

int Settings::getCartCountAtAChekpoint(int cp)
{
    if (cp < 0 || cp >= cartsCountAtCheckPoint.size()) {
        return 0;
    } else {
        return cartsCountAtCheckPoint[cp];
    }
}

void Settings::setCartSpeed(int cartId, int value)
{
    if (cartId >= cartSpeeds.count() ||  cartId < 0) {
        return;
    } else {
        cartSpeeds[cartId] = value;
    }
}

void Settings::setCartCountAtCheckpoint(int cp, int value)
{
    if (cp < 0 || cp >= cartsCountAtCheckPoint.size()) {
        return;
    } else {
        cartsCountAtCheckPoint[cp] = value;
    }
}
int Settings::getSimState() const
{
    return simState;
}

void Settings::setSimState(int value)
{
    if (simState != value) {
        int v = simState;
        simState = value;

        emit stateChanged(v, simState);
    }
}

int Settings::getMode() const
{
    return mode;
}

void Settings::setMode(int value)
{
    mode = value;
}



