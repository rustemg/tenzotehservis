#include "cart.h"
#include "src/settings.h"
#include "src/defs.h"

Cart::Cart(int id, AbstractCartView* view, QObject *parent) :
    QObject(parent),
    timer(this)
{
    this->id = id;
    state = CartState::NoJob;
    this->view = view;
    isWarehouseCart = (id == 0);
    target = getPosition();

    connect(&timer, SIGNAL(timeout()), SLOT(catchTimerTimeout()));
    connect(this, SIGNAL(targetReached()), SLOT(parsePosition()));
    connect(&Settings::current(), SIGNAL(stateChanged(int,int)),
            SLOT(processSimStateChange(int,int)));
}


void Cart::startMove()
{
    setState(CartState::Moving);
    timer.setInterval(400);
    timer.setSingleShot(false);
    timer.start();
}


void Cart::startMove(int target)
{
    setTarget(target);
    startMove();
}


void Cart::doSampleMove()
{
    bool finish = false;

    int k = 1;
    if (target < getPosition()) {
        k = -1;
    }

    int nextPos = getPosition() + k * Settings::current().getCartSpeed(id);
    if (k * nextPos >= k * target) {
        finish = true;
        nextPos = target;
    }
    view->setPosition(nextPos);

    if (finish) {
        timer.stop();
        emit targetReached();
    }
}


void Cart::parsePosition()
{
    if (isWarehouseCart) {
        parseWarehouseCartPosition();
    } else {
        parseWorkshopCartPosition();
    }
}

void Cart::processSimStateChange(int oldValue, int newValue)
{
    Q_UNUSED(oldValue)
    switch(newValue) {
    case Settings::STATE_RUN:
        timer.start();
        break;
    case Settings::STATE_PAUSE:
        timer.stop();
        break;
    case Settings::STATE_STOP:
        timer.stop();
        if (isWarehouseCart) {
            target = Y_POS_0;
            view->setPosition(Y_POS_0);
        } else {
            target = X_POS_BEGIN;
            view->setPosition(X_POS_BEGIN);
        }
        view->hideIcon();
        state = CartState::NoJob;
        break;
    }
}


void Cart::parseWarehouseCartPosition()
{
    switch(getPosition()) {
    case Y_POS_0:
        setState(CartState::NoJob);
        break;
    default:
        setState(CartState::Waiting);
        break;
    }
}


void Cart::parseWorkshopCartPosition()
{
    switch(getPosition()) {
    case X_POS_BEGIN:
        if (withCargo) {
            downloadCargo();
        } else {
            setState(CartState::NoJob);
        }
        break;
    default:
        setState(CartState::Waiting);
        break;
    }

}


void Cart::uploadCargo()
{
    setState(CartState::Uploading);
    view->showUploadIcon();
    timer.setSingleShot(true);
    if (isWarehouseCart) {
        timer.setInterval(Settings::current().getWarehouseCartUploadSpeed());
    } else {
        timer.setInterval(Settings::current().getWarehouseCartDownloadSpeed());
    }
    timer.start();
}


void Cart::downloadCargo()
{
    view->showDownloadIcon();
    setState(CartState::Downloading);
    timer.setSingleShot(true);
    if (isWarehouseCart) {
        timer.setInterval(Settings::current().getWarehouseCartDownloadSpeed());
    } else {
        timer.setInterval(1500);
    }
    timer.start();
}


void Cart::catchTimerTimeout()
{
    switch(getState()) {
    case CartState::Moving:
        doSampleMove();
        break;

    case CartState::Uploading:
        view->hideIcon();
        withCargo = true;
        if (isWarehouseCart) {
            setState(CartState::UploadFinished);
        } else {
            startMove(X_POS_BEGIN);
        }
        break;

    case CartState::Downloading:
        view->hideIcon();
        withCargo = false;
        if (isWarehouseCart) {
            setState(CartState::DownloadFinished);
        } else {
            setState(CartState::NoJob);
        }
        break;
        
    default:
        break;
    }
}


void Cart::setState(const CartState::CartState &value)
{
    if (state != value) {
        CartState::CartState s = state;
        state = value;
        emit cartStateChanged(s, value);
    }
}


void Cart::setTarget(int value)
{
    target = value;
}


int Cart::getId() const
{
    return id;
}


CartState::CartState Cart::getState() const
{
    return state;
}


int Cart::getTarget() const
{
    return target;
}


int Cart::getPosition() const
{
    return view->getPosition();
}
