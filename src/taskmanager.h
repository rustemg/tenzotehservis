#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include <QObject>
#include "src/gui/taskwidget.h"

class TaskManager : public QObject
{
    Q_OBJECT
public:
    explicit TaskManager(TaskWidget* taskWidget, QObject *parent = 0);
    int getCurrentTask();
    int getFutureTask();
    QVector<int> getFutureTasks();
    int taskCount();

public slots:
    void removeCurrentTask();

private:
    int getCartId(int pos);

private:
    TaskWidget* taskWidget;

};

#endif // TASKMANAGER_H
