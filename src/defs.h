#ifndef DEFS_H
#define DEFS_H

#define Y_POS_0 55
#define Y_POS_1 90
#define Y_POS_2 136
#define Y_POS_3 176
#define Y_POS_4 210

#define X_POS_BEGIN     80
#define X_POS_MID_1     120
#define X_POS_MIDDLE    190
#define X_POS_MID_2     205
#define X_POS_END       270

#endif // DEFS_H
