#include "taskmanager.h"

TaskManager::TaskManager(TaskWidget* taskWidget, QObject *parent) :
    QObject(parent)
{
    this->taskWidget = taskWidget;
}

int TaskManager::getCurrentTask()
{
    if (taskCount() == 0) {
        return 0;
    } else {
        return getCartId(0);
    }
}

int TaskManager::getFutureTask()
{
    if (taskCount() < 2) {
        return 0;
    } else {
        return getCartId(1);
    }
}

QVector<int> TaskManager::getFutureTasks()
{
    QVector<int> v;
    for (int i = 1; i < taskWidget->listWidget->count(); i++) {
        v.append(getCartId(i));
    }

    return v;
}

int TaskManager::taskCount()
{
    return taskWidget->listWidget->count();
}

void TaskManager::removeCurrentTask()
{
    taskWidget->listWidget->takeItem(0);
}

int TaskManager::getCartId(int pos)
{
    return taskWidget->listWidget->item(pos)->text().right(1).toInt();
}
