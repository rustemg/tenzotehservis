#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QVector>

class Settings : public QObject
{
    Q_OBJECT

public:
    static const int MODE_STANDART = 0;
    static const int MODE_NEO = 1;

    static const int STATE_STOP = 0;
    static const int STATE_PAUSE = 1;
    static const int STATE_RUN = 2;

public:
    static Settings& current()
    {
        static Settings theSingleInstance;
        return theSingleInstance;
    }

    int getMode() const;
    int getSimState() const;
    int getWarehouseCartUploadSpeed() const;
    int getWarehouseCartDownloadSpeed() const;
    int getCartSpeed(int cartId) const;
    int getCartCountAtAChekpoint(int cp);

signals:
    void stateChanged(int oldValue, int newValue);

public slots:
    void setMode(int value);
    void setSimState(int value);
    void setWarehouseCartUploadSpeed(int value);
    void setWarehouseCartDownloadSpeed(int value);
    void setCartSpeed(int cartId, int value);
    void setCartCountAtCheckpoint(int cp, int value);

private:
    int mode;
    int simState;
    int warehouseCartUploadSpeed;
    int warehouseCartDownloadSpeed;
    QVector<int> cartSpeeds;
    QVector<int> cartsCountAtCheckPoint;

private:
    explicit Settings(QObject *parent = 0);
    Settings(const Settings& root);
    const Settings& operator=(Settings&);
};

#endif // SETTINGS_H
