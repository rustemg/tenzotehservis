#ifndef CART_H
#define CART_H

#include <QObject>
#include <QTimer>
#include "src/gui/cart/abstractcartview.h"

namespace CartState {
enum CartState {NoJob, Waiting, Uploading, UploadFinished,
                Downloading, DownloadFinished, Moving};
}

class Cart : public QObject
{
    Q_OBJECT
public:
    explicit Cart(int id, AbstractCartView* view, QObject* parent = 0);
    CartState::CartState getState() const;
    int getId() const;
    int getPosition() const;
    int getTarget() const;

public slots:
    void startMove();
    void startMove(int target);

    void setState(const CartState::CartState &value);
    void setTarget(int value);

    void uploadCargo();
    void downloadCargo();

signals:
    void cartStateChanged(CartState::CartState oldValue, CartState::CartState newValue);
    void targetReached();

private slots:
    void catchTimerTimeout();
    void parsePosition();
    void processSimStateChange(int oldValue, int newValue);

private:
    void doSampleMove();
    void parseWarehouseCartPosition();
    void parseWorkshopCartPosition();

private:
    int id;
    int target;
    CartState::CartState state;
    bool withCargo;
    bool isWarehouseCart;
    QTimer timer;
    AbstractCartView* view;
};


#endif // CART_H
